import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Tecate Chronicles',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text('Chapter 1',
                  style: TextStyle(color: Colors.blueGrey[500],
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ],
            ),
          ),
        ActingWidget(),],
      ),
    );
    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.red, Icons.fastfood, 'Feed Me'),
          _buildButtonColumn(Colors.red, Icons.mood, 'Yappy'),
          _buildButtonColumn(Colors.red, Icons.directions_bike, 'Outdoors'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Hello Everyone, my name is Tecate. '
            'I am a trustworthy companion who loves to eat anything anyone near me drops '
            'and poop exactly where I am told not to.'

        , softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Introduction'),
        ),
        body: ListView(
            children: [
              Image.asset('images/tecate.jpg',
                width: 600,
                height: 340,
                fit: BoxFit.cover,
              ),
              titleSection,
              buttonSection,
              textSection
            ]
        ),
      ),
    );
  }
}

Column _buildButtonColumn(Color color, IconData icon , String label) {
  return Column (
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Icon (icon , color: color),
      Container(
        margin: const EdgeInsets.only(top: 8),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),

      ),
    ],

  );
}

class ActingWidget extends StatefulWidget{
  @override
  _ActingWidgetState createState() => _ActingWidgetState();
}

class _ActingWidgetState extends State<ActingWidget>
{
  bool _isActedOn = true;
  int actionCount = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.centerRight,
            icon: (_isActedOn? Icon(Icons.star) : Icon(Icons.star_border)),
            color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$actionCount'),
          ),
        ),
      ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if(_isActedOn)
      {
       actionCount += 1;
      }
    }
    );

  }
}


